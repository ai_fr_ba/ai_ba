import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
import joblib

df = pd.read_csv('laptops.csv')

label_encoder = LabelEncoder()
for col in ['CompanyName', 'TypeOfLaptop', 'ScreenResolution', 'Cpu', 'Ram', 'Memory', 'Gpu', 'OpSys']:
  df[col] = label_encoder.fit_transform(df[col])


df[['Inches', 'Weight']] = df[['Inches', 'Weight']].apply(lambda x: round(x, 1))
df[['Price']] = df[['Price']].apply(lambda x: round(x, 2))


X = df.drop(columns=['Price'])
y = df['Price']


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)



param1 = {'n_neighbors': range(1, 11)}
param2 = {'criterion': ['absolute_error', 'friedman_mse', 'poisson', 'squared_error'], 'max_depth': range(3, 16)}
param3 = {'n_estimators': range(100, 2001,100)}

param_list = [param1, param2, param3]

KNN = KNeighborsRegressor()
DT = DecisionTreeRegressor()
RF = RandomForestRegressor()

model_list = [KNN, DT, RF]
model_log = ["KNN", "DT", "RF"]

result = pd.DataFrame()

for i in range(len(model_list)):
  Grid=GridSearchCV(estimator=model_list[i], param_grid=param_list[i]).fit(X_train, y_train)
  globals()['Grid%s' % model_log[i]]=pd.DataFrame(Grid.cv_results_)
  result = pd.concat([result,globals()['Grid%s' % model_log[i]].sort_values('mean_test_score',ascending=False).head(1)])

result.insert(loc=0, column='Algorithm', value=model_list)
result


model = RandomForestRegressor(700)
model.fit(X_train, y_train)
y_pred = model.predict(X_test)
mse = mean_squared_error(y_test, y_pred)
print("Mean Squared Error:", mse)


joblib.dump(model, 'finalmodeltest.joblib', compress=True)











