from fastapi import FastAPI, HTTPException
import joblib
import numpy as np
from fastapi.middleware.cors import CORSMiddleware
from sklearn.preprocessing import LabelEncoder


global model
model = joblib.load("finalmodeltest.joblib")
app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE"],
    allow_headers=["*"],
)


@app.get('/')
def index():
    try:
        X_new = np.array([[5, 1, 17.0, 5, 6, 0, 15, 1, 0, 2.1]])
        prediction = model.predict(X_new)
        pred = (prediction.tolist())
        pred = float(prediction[0].round(2))
        return pred
    except Exception as e:
        return {"error": str(e)}


@app.post('/predict')
async def predict(data: dict):
    try:
        company = data.get('CompanyName')
        type = data.get('TypeOfLaptop')
        Inches = data.get('Inches')
        resolution = data.get('ScreenResolution')
        cpu = data.get('Cpu')
        ram = data.get('Ram')
        memory = data.get('Memory')
        gpu = data.get('Gpu')
        opSys = data.get('OpSys')
        weight = data.get('Weight')
        X_new = np.array([[company, type, Inches, resolution, cpu, ram, memory, gpu, opSys, weight]])
        prediction = model.predict(X_new)
        pred = (prediction.tolist())
        pred = float(prediction[0].round(2))
        return pred
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
